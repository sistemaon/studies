# DOCKER

studie, resume, example, guide

---

## O que é Docker?

Docker é uma **engine** de **container** _open source_. <br>
Disponibiliza o processo de _conteinerização_ rápida e sem risco.

Permitindo:
- Automação de pacotes.
- Entrega.
- Implantação.
```
De qualquer aplicação que seja leve, portátil.
Containers independente, que podem ser executados em qualquer lugar.
```

Docker container inclui componentes de software juntamente com todas as suas dependências, <br>
binários, bibliotecas, arquivos de configuração, scripts, etc...

Docker também permite testar o código e então implementar em produção o mais rápido o possível. <br>
Primatiamente, docker consiste em **Docker engine** e **Docker hub**.

Docker Engine: Consumação de _containers_ específicos como tabém de genéricos. <br>
Docker Hub: Repositório de imagens de _dockers_.

- **Containers**:
    - Representa sistema operacional virtualizado.
    - Leve.
    - Escalabidade e fornecimento em tempo real.
    - Performance nativa.
    - Isolament à nível de processo, portanto, menos seguro.

---

## Instalação

### Docker

**Atualizando sistema**
```zsh
sudo apt-get update
```

**Desinstalando versões antigas**
```zsh
sudo apt-get remove docker docker-engine docker.io containerd runc
```

**Configurando repositório**
```zsh
sudo apt-get update

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

**Adicionando chave GPG official do docker**
```zsh
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

**Verificando fingerprint da chave** (fingerprint - _impressão digital_)
```zsh
sudo apt-key fingerprint 0EBFCD88
```

**Configurando repositório estável**
```zsh
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

### Docker Engine

**Atualizando sistema e instalando última versão do docker engine e containerd**
```zsh
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io
```

**Verificando a instalação do docker engine**
```zsh
sudo docker run hello-world
```

_Mais informações_:
[instalando docker](https://docs.docker.com/engine/install/ubuntu/)

---

## Manipulando Containers do Docker

### Docker Imagens e Containers

Imagem docker é uma **coleção** de todos os _arquivos_ que **compoẽm** uma _aplicação de software_. <br>
Deve **originar** de um _imagem base_ de acordo com os **requisitos**. <br>
Cada vez que se **COMMIT** com um docker, cria se uma _nova camada_ na imagem do docker, <br>
mas imagem **original** e cada **camada** pré-existente permanece inalterada. <br>
Imagens docker são **componentes de construção** dos docker containers.

### Camada Docker

Camada Docker pode representar tanto _imagens_ **somente leitura** ou **leitura/gravação**. <br>
A _camada superior_ da **pilha/stack** de containers é **sempre** leitura/gravação que hospeda um docker conteiner.

### Docker Container

Container é **originado** de uma imagem somente leitura _através_ da **ação COMMIT**. <br>
Quando **START** um container, **refere se** uma imagem através do ID **exclusivo**. <br>
O Docker **pega/pull** a imagem **necessária** e a imagem **principal**. <br>
Continua a pegar todas as imagens principais até **alcançar** a imagem **base**.