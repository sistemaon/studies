
# Test TDD/BDD Unit Test

<br>

## Testando Microserviços Node.js.js

**Testar** é a atividade de **validar** a _aplicação_ que foi construída.

<br>

## Teste Funcional

Atividade que **consome** _tempo_ e que _não_ **recebe** a _atenção_ necessária enquanto desenvolve.

Como as companhias geralmente _evolui_:
- Alguém com a **ideia**.
- Alguém com a **construção**.
- Companhia **entra** para o mercado.

Indisponibilização de _tempo_ para _testar_ o **mínimo** requerido. <br>
Testes podem **tomar** até _40%_ do **tempo** de _desenvolvimento_.

**Automação**; _integração_ e testes _end-to-end_ é uma maneira de _automatizar_.

<br>

## Teste Unitário

Teste que **cobre** partes individuais de uma aplicação **desconsiderando** _integração_ com módulos. <br>
Por não haver _interação_ com módulos, **mocks** e **stubs** são utilizados para _simular_ respostas e _controlar_ o fluxo.

<br>

## Teste de Integração

Projetado para **verificar** a integração do _módulo_ no **ambiente** da _aplicação_. <br> 
Testar **unidades** do _négocio_, onde os dados são **salvos** no _database_, <br>
**chamadas** web services de terceiros ou qualquer outro tipo de _microservices_.

<br>

## Teste End-to-End

**E2E**; implantar a aplicação emitindo chamadas para execução do destino. <br>
Com foco em testar casos de uso.

<br>

## Pirâmide de Teste

#### Plano sadio de teste

![piramide de teste](./imgs/tddbdd00.png)

**Muito** teste _unitário_, **algum** teste de _integração_ e **pouco** teste _E2E_.

Maioria dos problemas são **flagrados** com testes _unitários_, por volta de 70% do teste, o mesmo é testes unitários. <br>

Testes de integração é _responsável_ por **capturar** problemas de integração <br>
como uma _conexão_ com o banco de dados, _envio_ de headers, _chamadas_ webservices. <br>
Por volta de 20% do teste, o mesmo é testes de integração.

E2E deve ser **limitado** e testar apenas os **principais** _fluxos_ da aplicação sem muito detalhe.

<br>

## Testando Microserviços Node.js

#### Chai

Biblioteca de _assertions_ (_asserções_) BDD/TDD que pode ser usada em <br>
conjunto com outras _bibliotecas_ para **criação** de testes de **alta qualidade**.

Asserção é uma **declaração** de _código_ que será **cumprida** ou _lancará_ um **error**. <br>
Exemplo;
```
3 should be equal to C
```
A declaração será **correta** se a variável _C_ **possuir** o valor _3_.

<br>

#### Interface Estilo BDD

Chai vem com dois tipos de **interface BDD**, <br>
questão de preferência por qual usar.

1. Interface **should**:
    - BDD Estilo.
    - Semelhante à linguagem natural.
    - Criação de asserção.
    - Decisão de sucesso ou erro.

```
myVar.should.be.a('number')
```

2. Interface **expect**:
    - BDD Estilo.
    - Semelhante ao should.
    - Sintaxe pouco diferente.
    - Definir expectativas que os resultados devem atender.

```
expect(myVar).to.equal(369)
```

Existem várias palavras em _linguagem natural_ que pode ser usado em testes usando **expect** e **should**.

- **expect** _fornece_ um _ponto de partida_ para o _chaining_ (_encadeamento_).
- **should** _estende_ assinatura _Object.prototype_ para _adicionar_ encadeamento em _cada_ objeto JavaScript.

<br>

#### Interface Asserção

A interface **assert** corresponde a biblioteca de asserções de testes, <br>
necessário ser **específico** sobre _o que_ testar. <br>
Fornece notação clássica de _afirmação de ponto_ (_assert-dot notation_).

Permite a **inclusão** uma _mensagem opcional_ como o _último parâmetro_, <br>
serão **incluídos** nas _mensagens de erro_, caso sua **afirmação** não seja aprovada.

```
// sem mensagem
assert.typeOf(myVar, 'number')

// com mensagem
assert.typeOf(myVar, 'number', 'myVar is not number type.')

// asserção igualdade
assert.equal(myVar, 963) 
```

<br>

#### Mocha

Segue o princípio de _teste desenvolvimento orientado a comportamento_ (behavior driven development testing (_BDDT_)). <br>
Descreve o _caso de uso_ da **aplicação** utilizando _asserções_ de uma **biblioteca**, verificando o **resultado** da execução. <br>
Define a _própria_ linguagem **específica** de domínio (domain specific language (_DSL_)) que é executado com **mocha**. <br>
Disponibiliza um **relatório** _compreensivo_ sobre o que está **acontecendo** nos testes.

```
describe('quando o cliente seleciona uma cor primária (rgb)', function(){
    it('deve retornar uma cor string', function() { 
        expect(selectColor()).to.be.an('string') 
    })
    it('deve retornar uma das cores (rgb)', function() { 
        selectColor().should.be.oneOf(['red', 'green', 'blue']) 
    })
    it('deve não ser nulo', function() { 
        expect(selectColor()).to.not.be.null
    })
    it('deve não ser indefinido', function() { 
        expect(selectColor()).to.not.be.undefined 
    })
})
```

<br>

#### Sinon

Teste _espião_ (_spy_), _stubs_ e _mocks_.

**Spy**; _retorno da chamada_ (_callback_) é executado com a lista correta de argumentos fornecidos.
```
describe("Quando o usuário realiza uma multiplicação", function() { 
  it("deve executar o callback como argumento", function() { 
    const callback = sinon.spy() 
    calculateMultiplication(3, 3, callback) 
    callback.called.should.be.true 
  }) 
}) 
```

```js
const callback = sinon.spy()
calculateMultiplication(3, 3, callback)
```
Criando o **spy** e injetando na **function** como **callback**. <br>
Não é apenas uma _função_ **criada** pelo sinon, mas um **objeto** _completo_ com **informações**.

<br>

**Stub**; Similiar ao _mocks_, permitindo **funções** _falsas_ para **simular** o **retorno** _requerido_.
```js
const randomRGB = () => {
   const RGBColors = ["red", "green", "blue"]
   const randomInt = Math.floor(Math.random() * 3)
   return RGBColors[randomInt]
}
```

```
describe("When randomRGB gets called", function() {
  it("Math#random deve ser chamado sem argumentos", function() {
    sinon.stub(Math, "random")
    randomRGB()
    console.log(Math.random.calledWith())
  })
})
```
É feito um _stubbed_ no **método** _Math#random_, <br>
causando algum tipo de _função_ vazia sobrecarregada, ou seja, não emite a chamada, <br>
registrando estatísticas **_sobre_** ou **_como_** foi chamado.

É necessário **restaurar** o método, pois _outros_ testes irão _ver_ <br>
o método _Math#random_ como _stub_, não como o **original**, <br>
e pode _levar_ o desenvolvimento de testes com informações inválidas.

Para **prevenir** isto, é preciso _utilizar_ os métodos **before()** e **after()** do Mocha.
```js
after( function () {
  Math.random.restore()
})
```
- **after(callback)** é executado **após** o **escopo** _atual_ (no **final** do bloco do **describe** (_descrição_)).

Uma das **características** interessante do _Sinon_ é a manipulação do **time** (tempo). <br>
Talvez seja necessário **executar** _tarefas periódicas_, ou <br>
**responder** após um certo _período_ da _ocorrência_ de um **evento**. <br>
Pode se **ditar** o _time_ como um dos _parâmetros_ de testes.
```js
let clock  
before( function() { 
  clock = sinon.useFakeTimers()
})
```
- **before (callback)** é executado **antes** do **escopo** _atual_ (no **início** do bloco do **describe** (_descrição_)).