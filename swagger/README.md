# SWAGGER

studie, resume, example, guide

---

## O que é OpenAPI ?

**Especificação OpenAPI** _(Especificação Swagger anteriormente)_ é um formato de descrição para _REST APIs_. <br>
Um arquivo OpenAPI permite descrever toda a API.

---

## O que é Swagger ?

**Swagger** é um conjunto de ferramentas open-source construído em torno da especificação _OpenAPI_ que auxília na _modelagem_, _construção_, _documentação_ e _consumo_ das REST APIs.

---

## Por que usar OpenAPI ?

A habilidade das APIs para descrever a própria estrutura é a raiz de toda essa incrivibilidade no OpenAPI. <br>
Uma vez escrito, a especifificação OpenAPI e ferramentas Swagger auxiliam no desenvolvimento.

---

## Estrutura Base

> Desenvolver em formato YAML ou JSON. <br>
> Palavras-chave são **case-sensitive**.

```yaml
openapi: 3.0.0
info:
  title: Sample API
  description: Optional multiline or single-line description in [CommonMark](http://commonmark.org/help/) or HTML.
  version: 0.1.9
servers:
  - url: http://api.example.com/v1
    description: Optional server description, e.g. Main (production) server
  - url: http://staging-api.example.com
    description: Optional server description, e.g. Internal staging server for testing
paths:
  /users:
    get:
      summary: Returns a list of users.
      description: Optional extended description in CommonMark or HTML.
      responses:
        '200':    # status code
          description: A JSON array of user names
          content:
            application/json:
              schema: 
                type: array
                items: 
                  type: string
```

---

## Metadata

> Na definicição incluir a versão.

```yaml
openapi: 3.0.0
```

---

## Info

> **title** nome da API. <br>
> **description** descrição sobre a API. <br>
> **version** versão da API (e _não_ do Open API). 

```yaml
info:
  title: Sample API
  description: Optional multiline or single-line description in [CommonMark](http://commonmark.org/help/) or HTML.
  version: 0.1.9
```

---

## Servers

> **servers** especifica o servidor da API e URL.

```yaml
servers:
  - url: http://api.example.com/v1
    description: Optional server description, e.g. Main (production) server
  - url: http://staging-api.example.com
    description: Optional server description, e.g. Internal staging server for testing
```

```yaml
https://api.example.com/v1/users?role=admin&status=active
\________________________/\____/ \______________________/
         server URL       endpoint    query parameters
                            path
```

---

## Paths

> **paths** define endpoints na API, e operações HTTP suportados pelos endpoints.

```yaml
paths:
  /users:
    get:
      summary: Returns a list of users.
      description: Optional extended description in CommonMark or HTML
      responses:
        '200':
          description: A JSON array of user names
          content:
            application/json:
              schema: 
                type: array
                items: 
                  type: string
```

---

## Parameters

> **parameters** operações com parâmetros via URL path (/users/{userId}), <br>
query string (/users?role=admin), headers (X-CustomHeader: Value) ou cookies (Cookie: debug=0).

```yaml
paths:
  /user/{userId}:
    get:
      summary: Returns a user by ID.
      parameters:
        - name: userId
          in: path
          required: true
          description: Parameter description in CommonMark or HTML.
          schema:
            type : integer
            format: int64
            minimum: 1
      responses: 
        '200':
          description: OK
```

---

## Request Body

> **requestBody** palavra-chave para descrever o conteúdo do body (corpo) e o media type (tipo).

```yaml
paths:
  /users:
    post:
      summary: Creates a user.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                username:
                  type: string
      responses: 
        '201':
          description: Created
```

---

## Responses

> **responses** operações que define possíveis códigos de status (HTTP response status codes).

```yaml
paths:
  /user/{userId}:
    get:
      summary: Returns a user by ID.
      parameters:
        - name: userId
          in: path
          required: true
          description: The ID of the user to return.
          schema:
            type: integer
            format: int64
            minimum: 1
      responses:
        '200':
          description: A user object.
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                    format: int64
                    example: 4
                  name:
                    type: string
                    example: Jessica Smith
        '400':
          description: The specified user ID is invalid (not a number).
        '404':
          description: A user with the specified ID was not found.
        default:
          description: Unexpected error
```

---

## Modelos Input e Output

> **components/schemas** define estrutura de dados comums utilizados na API. <br>
> Referenciados via **$ref** quando um **schema** é requerido.

<br>

**exemplo do objeto JSON**:
```json
{
  "id": 4,
  "name": "Arthur Dent"
}
```

**representado como:**
```yaml
components:
  schemas:
    User:
      properties:
        id:
          type: integer
        name:
          type: string
      # Both properties are required
      required:  
        - id
        - name
```

**referenciado no schemas do request body e response body:**
```yaml
paths:
  /users/{userId}:
    get:
      summary: Returns a user by ID.
      parameters:
        - in: path
          name: userId
          required: true
          type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
  /users:
    post:
      summary: Creates a new user.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
      responses:
        '201':
          description: Created
```

---

## Autenticação

> **securitySchemes** e **security** são palavras-chave utilizados para descrever <br>
> métodos de autenticação usados na API.

> Métodos de autenticação suportados são:
> - HTTP authentication: Basic, Bearer, etc... <br>
> - API key como header ou parâmetro query ou cookies <br>
> - OAuth2 <br>
> - OpenID Connect Discovery

```yaml
components:
  securitySchemes:
    BasicAuth:
      type: http
      scheme: basic
security:
  - BasicAuth: []
```

---

> Referências: 
> - [Swagger docs specification about](https://swagger.io/docs/specification/about/)
> - [Swagger](https://swagger.io/)
