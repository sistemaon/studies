
# Google Cloud Platform GCP

## Introdução Google Cloud Platform

Quatro categorias **principais** de serviços em nuvem:
- **Compute** _computação_.
- **Storage** _armazenamento_.
- **Big data**.
- **Machine learning**.

> Segurança e rede na nuvem é essencial para que as peças se unem de maneira robusta e de baixa latência.

<br>

Opções para _computação_ e _hospedagem_:
- Ambiente **serverless** - _Function Engine_.
- Gerenciamento de **aplicativo** - _Apps Engine_.
- Flexibilidade em **containers** - _Kubernetes Engine_.
- Controle e flexibilidade na **infraestrutura** em nuvem - _Compute Engine_.

> Numa ponta se tem responsabilidades para gerenciar recursos, _Compute Engine_, <br>
> na outra ponta, _Function Engine_ se tem responsabilidades de gerenciar serviços em ambientes _serverless_.

![Responsabilidade Operacional](./imgs/gcp00.png)

> Não é possível utilizar efetivamente recursos da nuvem sem utilizar serviços de rede na nuvem.

---

## Conceitos Computação em Nuvem

Computação em nuvem é sobre entrega perfeita dos _serviços_.

```
Incluso:
- Infraestrutura (servidores virtuais).
- Armazenamento em grande escala.
- Banco de dados.
- Networking.

Plataforma para:
- Big data.
- Análise avançada (analytics).
- Inteligência artificial (a.i).
```

> Serviços providenciam inovação ágil através de infraestrutura flexível e _deployment_ rápido de recursos, beneficiando de _economias de escala_. <br>
> Ajuda a diminuir despesas, executa operações e infraestruturas mais efetivos e econômico. <br>
> Capacidade de expansão ao atender requisitos de negócio.

<br>

```
Princípios fundamentais:
- Econômico.
- Altamente ativo para desenvolvimento rápido.
- Altamente competente para deploy.
- Altamente produtivo para executar aplicações.
- Altamente eficaz para armazenar dados.
- Escalável.
- Dinâmico.
- Desempenho.
- Entrega de serviços globalmente.
- Serviços e produtos próximos do cliente final.
- Confiabilidade.
- Backup.
- Recuperação.
- Mirrored (espelhamento).
- Produtividade.
- Redução nas tarefas manuais (instalação, configuração, patches de segurança, provisionamento, etc...)
- Foco em solução e organização.
```

> Reduz qualquer custo inicial de investimento em servidores e outras infraestruturas. <br>
> Reduz carga operacional, desgaste de tempo. <br>
> Reduz ocupação desnecessárias.

> Escala elástica (aumento e dedução de consumo como requerimento). <br>
> Acesso a preços razoáveis a alto desempenho. <br>
> Recursos de infraestrutura presentes o tempo todo.

> Distribuiçao de conteúdos. <br>
> Fornece load balancing globalmente. <br>
> Resulta em menor latência e economia de escala.

> Continuidade de negócio fácil e econômico. <br>
> Dados espelhados em múltiplos locais. <br>
> Solução ágil e custo reduzido (**local** _x_ **nuvem**).

> Remoção de tarefas manuais e deveres de suporte. <br>
> Equipe adquirindo mais tempo em alcançar metas. <br>
> Estratégia mais inovadora e orientada para o negócio.

---

## Google Cloud Platform

Preocupação e interesse são _chaves_ para **segurança** e **gerenciamento**.

O modelo _finaceiro_ da nuvem _câmbeia_ a _carga_ de **capital único** (_CapEX_) <br>
para **custos operacionais recorrentes** (_OpEX_).

#### Colocação

Muitas das médias e grandes empresas já estavam no caminho para a nuvem. <br>
Como por exemplo, **colocação** (_colocation_) esteve presente enquanto companhias esforçou <br>
ao máximo para reduzir custos do **data center** _compartilhando_ instalações. <br>

Grandes empresas compensariam o custo operacional do _data center_ prestando serviço de colocação <br>
para empresas menores com uma solução **viável** atendendo necessidade de ambas as partes. <br>

Várias pequenas empresas preferiam **colocar** em vez de **iniciar** um investimento escasso e **administrar** _intensivamente_. <br>

**Colocação** simplesmente é alugar um espaço num local compartilhado da organização. <br>

> Dirigir um data center não é carro-chefe para muitas empresas. <br>
> Colocação libera o capital para mais iniciativa estratégica e essencial para o negócio. <br>
> Pesquisa, desenvolvimento e melhorias no negócio, produtos e serviços.

#### Virtualização

Virtualização permitiu **produzir** _mais_, **fazer** _mais_ com _menos_ servidores <br>
e outros componentes de infraestrutura. <br>

Virtualização **correspondia** a uma _arquitetura_ data center _físico_. <br>
Drástica **redução** de servidores _físicos_. <br>

Possibilita a utilização de recursos mais **eficiente** e **flexível**. <br>
Capacidade de **execução** de **várias** aplicações de recurso baixo. <br>

Função **essencial** para gerenciamento de **servidores** e **network**. <br>

#### SDN

**Software Defined Networks** modelo em que permite _todos_ os serviços são <br>
provisionados automaticamente de forma **proativa**. <br>

Perfeitamente **configurado** para **otimização** da _infraestrutura_. <br>
Execução de **_stacks_**, **ambiente** e **aplicações**. <br>

_Google_ **criou** as _próprias_ redes globais SDN, **desenvolveram** roteadores _próprios_, <br>
switches e servidores para **atender** os _próprios_ **critérios** em todo o mundo. <br>

#### SaaS

**Software as a Service** é um serviço em que _capacita_ o fornecimento de software na nuvem. <br>
Redução de **mão de obra** e necessidade de **especialistas** em aplicações. <br>

#### IaaS

**Infrastructure as a Service** relacionado ao fornecimento de infraestrutura na nuvem. <br>
Adquira **recursos** _sob_ **demanda** ou **reserve** os. <br>
**Escalar** para **atender** e **obter** recursos necessários. <br>

#### PaaS

**Platform as a Service** disponibiliza serviços de plataforma na nuvem. <br>
Consiste em **hospedagem** e **implementação** de _hardware_ e _software_. <br>
Provendo **aplicações**, **serviços** e **produtos** por meio da internet. <br>

---

<br>

# Introdução Segurança na Nuvem

## Segurança por Design

**Protege** acesso _físico_ nas instalações implementando camadas **segurança**. <br>
**Criação** de _chips_ customizados. <br>

<br>

## Projetos e Pastas

GCP regras básicas sobre projetos:
- Todo o **recurso** GCP que _deploy_ deve pertencer a um **único** projeto.
- Projetos são **mecanismos** para _habilitar_ e usar **serviços** como gerenciar, criar, remover, adicionar, atualizar.
- Compartimento **separado** por _cada_ projeto e _cada_ recurso **pertence** a exatamente um.
- Projetos podem haver **diferentes** _proprietários_ e _usuários_, são _construídos_ e _gerenciáveis_ **separadamente**.
- Projetos possuem _nome_ e _ID_ que são **assinados**. O ID do projeto é _permanente_, identidate _imutável_ e **único**. 